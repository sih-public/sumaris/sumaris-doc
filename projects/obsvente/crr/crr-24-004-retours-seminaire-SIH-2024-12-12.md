# Retours formation séminaire SIH - Opus ObsVentes

### 12/12/2024

> Présents :
> Environ 30 participants

> Déroulé
> Présentation MOE de la gestion de projet
> Présentation MOE du prototype

> Condition d'utilisation
> Dégradée : réseau instable, perte de connexion VPN


- Formulaire : 
  - Etat et présentation : prévoir des valeurs par défaut
  - Auto renseigner au maximum les champs (régionalisation, valeur unique possible...)
  - Zone de pêche du lot : non obligatoire (car l'info n'est pas encore connu) et prévoir de mettre plusieurs zones
  - L'heure de fin ne doit pas être obligatoire en mode tablette
  - Rajouter un mode pour assigner plusieurs espèces à un navire


Cas particuliers (DOM) : 
- Cas des caisses de mélange : peut-on déroger à la liste d'espèces scientifiques par rapport à l'espèce commerciale (pouvoir lever la règle espèce commerciale / espèce scientifique)?

- Régionalisation : 
  - Régionaliser la liste des espèces 

> 11/02/2025 : Vérifier si ticket déjà créé

- Référentiel : 
  - Avoir un référentiel espèces / catégories
  - Restreindre les valeurs par espèce lors de la création d'un lot
  - Liste des espèces tirée au sort : mettre en place le lien entre WAO et Harmonie, se baser sur la date et le lieu (et la ligne de plan ?)

Synthèse de saisie : 
  - Pouvoir voir le nombre d'individus mesurés et la somme des poids. Avoir une vue en graphique nb indiv / classe de taille
  - Prévoir une vue synthétique de la saisie et que cette vue soit cliquable
  - Dans le menu de gauche, détailler les lots au lieu de mettre juste le libellé "Lots" - pb de perf ?
  - Sur l'écran des mesures individuelles, ajouter la somme des poids car l'objectif c'est soit 30 individus soit 150kg (OM)
  - Warnings si on sort des bornes en taille et en poids


Mode tablette : 
- Pertinence du mode tablette ? Pas forcément nécessaire dans la V1, l'objectif est l'isofonctionnel pour débrancher allegro


A détailler : 
- Pouvoir ajouter une espèce même en hexagone ?
- Notion d’occurrence probable : pouvoir activer  désactiver
- Si dans Wao la sortie est créée, créer la sortie dans Opus
- Prévoir un import des mensurations réalisées sur un autre système
- Boucle OM : Espèce puis présentation puis taille. Pouvoir choisir l'espèce à chaque boucle
- Reprendre le concept d'obsdeb pour proposer les derniers lots saisis    



> Prévoir un atelier avec les saisisseurs métropole et outremer pour valider les écrans et la fonctionnelle
> Prévoir un atelier avec les saisisseurs pour déterminer les tablettes à utiliser dans les conditions en criée (froid, mains sales, batterie, poids)