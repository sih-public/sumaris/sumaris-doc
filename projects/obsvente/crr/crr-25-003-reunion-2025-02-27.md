
# Réunion 27/02/2025

## Cadrage ObsVentes - Observateurs

--- 

### Ordre du jour

- [Planning](/projects/obsvente/not/images/refonte-obsventes-planning-sprints.svg)

- [Retours sur la démonstration](/projects/obsvente/crr/crr-24-004-retours-seminaire-SIH-2024-12-12.md) du séminaire SIH 

- Présentation de l'application en cours de développement

- Echanges sur les améliorations/adaptations à réaliser

- Plannification des prochains jalons

---

### Modifications depuis le séminaire SIH

- Paramétrage de la **zone de compétence** : Implémentée

- **Sortie**

  - **Sortie** > Ecran "Détails" : RAS
    
    > Observateurs : RAS 

  - **Sortie** > Ecran Echantillonnage  : RAS

    > Observateurs : RAS

---

- **Vente**

  - **Vente** > Ecran "Détails" : 
    - Possibilités de saisir plusieurs métiers
      - 1 seul principal par défaut
    - Possibilités de saisir plusieurs zones de pêche

      > Observateurs : 
      > 
      > - Pas de zone de pêche connu parfois donc pas obligatoire
      > - Métier pas obligatoire
      > - Zone de pêche : Zone CUEM (pas besoin d'affiner)   

  - **Vente** > Ecran "Lots" : RAS

    > Observateurs :
    > - Le poids total est le poids de toutes les caisses de la débarque. Le poids échantillonné est le poids de la caisse
      > - Renommer poids total ?
      > - Problème avec les raies (plusieurs espèces scientifiques derrière une espèce commerciale) : Utilisation du filtre sur les espèces scientifiques : Validé par les utilisateurs
      > - Poids affiché : poids RTP régionalisé (à vérifier MOE)
      > - Enrichir les poids RTP pour les espèces (travail en cours sur les RTPs, action MOA)
      > - Méditérranée : Ne pas saisir le poids de la débarque (poids total) ? Le modèle impose t il de calculer le poids total ? Si oui, calculer un poids fictif
      >   - Marion valide avec Norbert si Poids total absent ?
      >   - Règle sur la zone Méditérranée ? Stratégie Méditérranée à faire ?
      >       - Rajouter une case à cocher "Poids total inconnu" pour tout le monde 
      >       - Coché par défaut si Méditérranée
      >       - Décoché si autre façade

    > Méditérranée : Protocole pas simple car une espèce à échantilloner (plusieurs caisses de petis/gros) peuvent être sur plusieurs navires
    > - Or dans le protocole, une espèce est rattachée à un navire
    > - Possibilité de créer 2 ventes sur une espèce ?
    > - Le navire doit apparaitre après l'espèce (paramétrable dans l'application)

---

  - **Vente** > Ecran des "Mesures"
    - Photo à chaque espèce
    - Filtre des espèces sur mensurations et dénombrement : cas des caisses de mélange
    - En mode dénombrement, doit on voir les mensurations précédemment saisi (sinon sensation d'avoir écrasé des données) ?

    > Observateurs :
    > - TODO : Figer les entêtes (Longueur, male, Female, ...)
    > - Ok pour le mode dénombrement
    > - Avoir les minx/max de renseignés pour chaque espèce (maintenir le référentiel des minx/max)
    > - Prévoir un point avec Armelle pour auto-alimenter le référentiel min/max (en V2)
    > - Prévoir de réafficher les précédentes mensurations quand on à valider le dénombrement
    >   - MOE/Dév : Non, on masque les mensurations dont l'espèce scientifique ou la mensuration est en dehors de la plage mais on avertit l'utilisateur par un warning
    >   - Sous Dénombrement : "Des données en hors classe de taille existent mais sont masquées"
    > - Reprendre le max pour le dénombrement
  

--- 

### Prochain Observateurs Métropole point le 27/03/2025

- Livrer l'application dans l'environnement Opus
    - Fournir l'application aux utilisateurs pour avoir des retours le 27/03/2025
    - Prévoir une réunion de présentation aux utilisateurs
    - Prévoir un point avec les utilisateurs de Méditérranée
  
- Echanger sur les mensurations lors de la réunion

- Echanger sur les contrôles sur les données/vérifications lors de la réunion

