# Sous-projets

* [ADAP](projects/adap/index.md) : Auto-échantillonnage et contrôles en criée - LPDB
* [APASE](projects/apase/index.md) : Tests de sélectivité de chaluts - Aglia/LPDB/Ifremer
* [IMAGINE](projects/imagine/index.md) : Collecte et suivi des paramètres biologiques - Ifremer
* [Ichtyomètre](projects/ichthyometer/index.md) : Ichtyomètre bluetooth (Gwaleen) - Aptatio/Ifremer
* [ObsMer](projects/obsmer/index.md) : Observation des captures en mer - Ifremer
* [ObsVentes](projects/obsvente/index.md) : Observation des ventes en criées - Ifremer
* [Activité](projects/activity-calendar/index.md) : Calendrier d'activité mensuelle - Ifremer
* [SFA](projects/sfa/index.md) : Observation des captures au débarquement - SFA
* [PIFIL & DOLPHINFREE](projects/pifil/index.md) : Captures accidentelles cétacés - DGAMPA/CNPMEN/Ifremer/OP
* [EOS](projects/eos/index.md) : (Elasmobranches On Shore) Collecte de données biologiques et halieutiques sur les elasmobranches exploités par les pêcheries de France métropolitaine - MNHN
