# Documents fournis

- Protocole de suivi EOS :
  - Version **novembre 2023** : [FR (PDF)](projects/eos/doc/Protocole_Suivi_EOS_2024.pdf) - [EN (PDF)](projects/eos/doc/EOS_sampling_protocol_2024.pdf)
- Manuel d'utilisation de la base DCFM :
  - Version **2022** : [FR (PDF)](projects/eos/doc/Manuel_d_utilisation_de_la_base_DCFM_2023.pdf)
- Liste des besoins de l'application :
  - Version **novembre 2024** : [FR (Word)](projects/eos/doc/Liste_besoin_application_e-is.docx)