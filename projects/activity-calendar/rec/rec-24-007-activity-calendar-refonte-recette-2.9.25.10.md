# Activity Calendar - Recette MOE/MOA

## Retours de recette MOA - release 2.9.25.10


### Calendriers d'activité > Import

- [ ] Toujours l'erreur du traitement qui tourne en boucle sans se terminer [#830](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/830)

### Calendriers d'activité > Calendrier

- [ ] Il y a un décalage d'un jours sur le contrôle du nombre de jours des jours de mer et des jours de pêche [#886](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/886)
  - Exemple : Sur Février 2023,on ne peut pas saisir 28 jours mais 27
  - Exemple : Sur Novembre 2023, on ne peut pas saisir 30 jours mais 29 


- [ ] Après avoir copié/collé toute la prédoc dans le calendrier (source sacrois), quand on fait "vider le calendrier", ça ne fonctionne pas [#895](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/895)


- [ ] Le ctrl+C/ctrl+V de toute la ligne jours/mer vers jours/peche ne fonctionne pas. Par le menu contextuel non plus [#896](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/896)


### Calendriers d'activité > Métier

- [ ] Comment passe-t-on d'une ligne à l'autre au clavier dans le tableau des caractéristiques ? les flèches utilisées dans le calendrier ne fonctionnent pas car elles font monter/descendre les valeurs saisies.
  - _MOE : pas de possibilité de passer au métier suivant par le clavier_
- [ ] La touche tabulation va vers les onglets de l'écran.
  - _MOE : La touche tabulation permet de naviguer de gauche à droite dans les cellules_ [#887](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/887)


### Calendriers d'activité > Régionalisation 

- [ ] Après la saisie de la zone de pêche, aucun gradient de côte n'est proposé.

- [ ] Les gradients côte semblent tous sur étranger, même ceux qui devraient être côtiers.

Ticket couvrant ces 2 items : [#894](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/894)



