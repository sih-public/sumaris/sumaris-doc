# Opus Calendrier d'activité - Livraison release 2.9.30 en exploitation

## Versions applicatives 

Application Opus - Calendrier d'activité en production le 06/02/2025 11h30

- release livrée du pod **2.9.30.1**
- release livrée de l'app **2.9.30**

--- 

## Tickets livrés 

[Bugs issues](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=ACTIFLOT&milestone_title=2.9.30&first_page_size=20)

| **Issue**                                                                         | Description                                                                                        | **Ticket OTRS**                                                                        | 
|-----------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| Bug [#895](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/895) | Calendrier - Bug action vider calendrier                                                           | -                                                                                      |
| Bug [#898](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/895) | Calendrier > Vidage d'un mois impossible                                                           | -                                                                                      |
| Bug [#899](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/899) | Copie d'un mois de prédoc (Erreur ORA - violation contrainte unique GEAR_USE_FEATURES_UNIQUE_KEY2) | -                                                                                      |
| Bug [#914](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/914) | Erreur dans les logs en production                                                                 | -                                                                                      |
| Bug [#916](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/916) | Importation > Suppression des données sur une mise à jour                                          | [500420](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=500420) |
| Bug [#917](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/917) | Rapport d'avancement/Nb enquêtes directes                                                          | [500520](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=500520) |
