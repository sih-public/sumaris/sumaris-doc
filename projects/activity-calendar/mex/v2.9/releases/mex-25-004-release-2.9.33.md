# Opus Calendrier d'activité - Livraison release 2.9.33 en exploitation

## Versions applicatives 

Application Opus - Calendrier d'activité en production le xx/03/2025 11h30

- release livrée du pod **2.9.33**
- release livrée de l'app **2.9.33**

--- 

## Tickets à livrer 

- [Bugs issues 2.9.33](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=ACTIFLOT&milestone_title=2.9.33&first_page_size=20)

| **Issue**                                                                         | Description                                                                                                                                  | **Ticket OTRS**                                                                          | Recette | 
|-----------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|---------|
| Bug [#883](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/883) | Calendrier d'activité > Calendrier > Erreur sur FISHING_AREA_UNIQUE_KEY cas particulier copié/collé                                          | [504391](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=504391)   | OK      |
| Bug [#897](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/897) | Calendrier d'activité > Paramètres > Régionalisation non sauvegardée                                                                         | -                                                                                        | OK      |
| Bug [#923](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/923) | Calendrier d'activité > Calendrier > Coller un gradient hors zone                                                                            | [501167](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=501167)   | OK      |
| Bug [#926](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/926) | Calendrier d'activité > Import de la feuille de route > Problème de mise à jour                                                              | -                                                                                        | OK      |
| Bug [#951](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/951) | Calendrier d'activité > Formulaire Vierge > Bloc Métier/Zone de pêche absent                                                                 | -                                                                                        | OK      |
| Bug [#967](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/967) | Calendriers d'activité > Navires : Contenu de la liste déroulante incorrecte (si requête trop longue)                                        | -                                                                                        | OK      |

