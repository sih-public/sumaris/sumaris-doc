# Opus Calendrier d'activité - Livraison release 2.9.32 en exploitation

## Versions applicatives 

Application Opus - Calendrier d'activité en production le 25/02/2025 11h30

- release livrée du pod **2.9.32**
- release livrée de l'app **2.9.32**

--- 

## Tickets à livrer 

- [Bugs issues 2.9.32](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=ACTIFLOT&milestone_title=2.9.32&first_page_size=20)

| **Issue**                                                                         | Description                                                                                                | **Ticket OTRS**                                                                          | Recette                             | 
|-----------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|-------------------------------------|
| Bug [#936](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/936) | Calendrier d'activité > Rapport d'avancement vide pour quartier SM                                         | [503592](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=503592)   | OK                                  |
| Bug [#939](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/939) | Calendrier d'activité > Métiers > Caractéristiques engin non récupérées (Fuseau horaire de La Réunion)     | [501806](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=501806)   | OK                                  |
| Bug [#942](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/942) | Calendrier d'activité > Rapport avancement > Dernière page tronquée                                        | [504317](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=504317)   | OK                                  |
| Bug [#946](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/946) | Calendier d'activité > Général > Problème de saisi de commentaire                                          | [504519](https://otrs.ifremer.fr/otrs/index.pl?Action=AgentTicketZoom;TicketID=504519)   | OK                                  |

