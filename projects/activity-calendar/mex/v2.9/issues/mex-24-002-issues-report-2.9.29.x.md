# Opus Calendrier d'activité - Issues restantes dans la version 2.9.29.x en exploitation

## Versions applicatives 

Application Opus - Calendrier d'activité en production le 20/01/2025

- release actuelle du pod **2.9.29.1**
- release actuelle de l'app **2.9.29.4**
- release corrective : **2.9.29.x**

- Communication sur l'ouverture de la saisie le 23 Janvier 2025

--- 

## Tickets restants/ouverts sur la 2.9.29.4

- Forecast release : 2.9.29.5, 2.9.29.6

[Bugs issues](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/?sort=created_date&state=opened&label_name%5B%5D=ACTIFLOT&not%5Blabel_name%5D%5B%5D=Done&not%5Bmilestone_title%5D=3.0.0&not%5Bmilestone_title%5D=2.10&not%5Bassignee_username%5D%5B%5D=cd061a4&not%5Bassignee_username%5D%5B%5D=vf22ca9&first_page_size=20)


| **Issue**                                                                         | **Goal**                                                  | **Priority** | **Release** | **Assignee** | 
|-----------------------------------------------------------------------------------|-----------------------------------------------------------|--------------|-------------|--------------|
| Bug [#872](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/872) | Copie prédoc (régionalisation)                            | major        | 2.9.29.30   | DEV          |
| Bug [#914](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/914) | Erreur dans les logs                                      | lower        | 2.9.29.30   | DEV          |
| Bug [#894](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/894) | Gradients de côte non adaptés                             | major        | 2.9.29.30   | DEV          |
| Bug [#875](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/875) | Warning régionalisation prédoc                            | major        | 2.9.29.30   | DEV          |
| Bug [#791](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/791) | delete sur la vue SIH2_ADAGIO_DBA_SUMARIS_MAP.STRATEGY    | major        | 2.9.29.30   | MOE          |
| Bug [#915](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/915) | Indexation ElasticSearch partielle en production          | major        | 2.9.29.30   | MOE          |
| Bug [#916](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/916) | Importation > Suppression des données sur une mise à jour | severe       | 2.9.29.30   | MOE          |


## Tickets ouverts pour la V2

- Forecast release : 2.10

[Bugs issues](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/?sort=created_date&state=opened&label_name%5B%5D=ACTIFLOT&milestone_title=2.10&first_page_size=20)

| **Issue**                                                                         | **Goal**                                                   | **Priority** | **Release** | **Assignee* | 
|-----------------------------------------------------------------------------------|------------------------------------------------------------|--------------|-------------|-------------|
| Bug [#568](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/568) | Poc Commentaire                                            | lower        | 2.10        | MOE         |
| Bug [#787](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/787) | Problème de demande de reconnexion                         | lower        | 2.10        | MOE         |
| Bug [#837](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/837) | Affichage de 2 listes déroulantes en simultané             | lower        | 2.10        | MOE         |
| Bug [#838](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/838) | Pb d'affichage prédoc avec en-têtes colonnes figées        | lower        | 2.10        | DEV         |
| Bug [#842](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/842) | Problème d'affichage de l'outil de sélection des couleurs  | lower        | 2.10        | DEV         |
| Bug [#843](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/843) | Modifier la section "A propos" et "Mentions légales"       | lower        | 2.10        | MOE         |
| Bug [#848](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/848) | Ajouter "Organisme observateur"                            | lower        | 2.10        | DEV         |
| Bug [#851](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/851) | Bug nom navire                                             | lower        | 2.10        | DEV         |
| Bug [#857](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/857) | Visuel mois non saisissables                               | lower        | 2.10        | DEV         |
| Bug [#858](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/858) | Visualisation prédoc selon les droits                      | lower        | 2.10        | DEV         |
| Bug [#880](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/880) | Gestion des droits "ALLEGRO_SUPER_UTILISATEUR"             | lower        | 2.10        | DEV         |
| Bug [#903](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/903) | Ajouts de liens (Formulaire guichet et manuel utilisateur) | lower        | 2.10        | MOE         |
| Bug [#907](https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/907) | Section Informations légales                               | lower        | 2.10        | MOE         |
