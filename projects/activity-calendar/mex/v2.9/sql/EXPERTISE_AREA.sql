REM INSERTING into SIH2_ADAGIO_DBA.EXPERTISE_AREA, SIH2_ADAGIO_DBA.EXPERTISE_AREA2LOCATION, SIH2_ADAGIO_DBA.EXPERTISE_AREA2LOCATION_LEVEL

-- ActiFlot : Liste des zones de competences, pour la régionalisation dans les logiciels
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'CRS', 'Corse', sysdate, current_timestamp, 1);
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'GLP', 'Guadeloupe', sysdate, current_timestamp, 1);
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'GUY', 'Guyane', sysdate, current_timestamp, 1);
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'REU', 'La Réunion', sysdate, current_timestamp, 1);
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'MTQ', 'Martinique', sysdate, current_timestamp, 1);
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'MYT', 'Mayotte', sysdate, current_timestamp, 1);
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'MED', 'Méditerranée', sysdate, current_timestamp, 1);	
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'ATL', 'Mer du nord manche atlantique', sysdate, current_timestamp, 1);	
insert into EXPERTISE_AREA (ID, LABEL, NAME, CREATION_DATE, UPDATE_DATE, STATUS_FK) values (EXPERTISE_AREA_SEQ.nextval, 'SPM', 'Saint Pierre et Miquelon', sysdate, current_timestamp, 1);

-- ActiFlot : Associations des zones de competences avec les lieux (terrestre ou en mer)
-- Corse
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 394 from EXPERTISE_AREA where LABEL='CRS';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 391 from EXPERTISE_AREA where LABEL='CRS';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4022 from EXPERTISE_AREA where LABEL='CRS';
-- Guadeloupe
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 430 from EXPERTISE_AREA where LABEL='GLP';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4020 from EXPERTISE_AREA where LABEL='GLP';
-- Guyane
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 423 from EXPERTISE_AREA where LABEL='GUY';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4020 from EXPERTISE_AREA where LABEL='GUY';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4024 from EXPERTISE_AREA where LABEL='GUY';
-- La Réunion
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 433 from EXPERTISE_AREA where LABEL='REU';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4028 from EXPERTISE_AREA where LABEL='REU';
-- Martinique
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 425 from EXPERTISE_AREA where LABEL='MTQ';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4020 from EXPERTISE_AREA where LABEL='MTQ';
-- Mayotte
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 424 from EXPERTISE_AREA where LABEL='MYT';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 429 from EXPERTISE_AREA where LABEL='MYT';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4028 from EXPERTISE_AREA where LABEL='MYT';
-- Méditerranée
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 408 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 410 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 420 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 415 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 413 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 419 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 394 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 391 from EXPERTISE_AREA where LABEL='MED';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4022 from EXPERTISE_AREA where LABEL='MED';
-- Atlantique
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 386 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 387 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 388 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 389 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 390 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 392 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 393 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 395 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 396 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 397 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 398 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 399 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 400 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 401 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 402 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 403 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 404 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 405 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 406 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 407 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 409 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 411 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 412 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 414 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 416 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 417 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 418 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 421 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 422 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 432 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 435 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 436 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 4018 from EXPERTISE_AREA where LABEL='ATL';
-- Ajouts pour les ports d'Irlande, Grande Bretagne, Espagne et Danemark
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 9 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 10 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 19 from EXPERTISE_AREA where LABEL='ATL';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 35 from EXPERTISE_AREA where LABEL='ATL';

-- Saint Pierre et Miquelon
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 434 from EXPERTISE_AREA where LABEL='SPM';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 12172 from EXPERTISE_AREA where LABEL='SPM';
insert into EXPERTISE_AREA2LOCATION (EXPERTISE_AREA_FK, LOCATION_FK) select ID, 12174 from EXPERTISE_AREA where LABEL='SPM';

-- ActiFlot : Associations des zones de competences avec les niveaux de lieu (par défaut)

-- Corse|142;143;144
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,142 from EXPERTISE_AREA where LABEL = 'CRS';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,143 from EXPERTISE_AREA where LABEL = 'CRS';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,144 from EXPERTISE_AREA where LABEL = 'CRS';

-- Guadeloupe|158;159
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,158 from EXPERTISE_AREA where LABEL = 'GLP';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,159 from EXPERTISE_AREA where LABEL = 'GLP';

-- Guyanne|154;156;157;161
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,154 from EXPERTISE_AREA where LABEL = 'GUY';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,156 from EXPERTISE_AREA where LABEL = 'GUY';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,157 from EXPERTISE_AREA where LABEL = 'GUY';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,161 from EXPERTISE_AREA where LABEL = 'GUY';

-- La Réunion|151;152;105
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,151 from EXPERTISE_AREA where LABEL = 'REU';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,152 from EXPERTISE_AREA where LABEL = 'REU';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,105 from EXPERTISE_AREA where LABEL = 'REU';

-- Martinique|158;159
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,158 from EXPERTISE_AREA where LABEL = 'MTQ';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,159 from EXPERTISE_AREA where LABEL = 'MTQ';

-- Mayotte|105;165;166
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK)select ID,105 from EXPERTISE_AREA where LABEL = 'MYT';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK)select ID,165 from EXPERTISE_AREA where LABEL = 'MYT';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK)select ID,166 from EXPERTISE_AREA where LABEL = 'MYT';

-- Méditerranée|142;143;144
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,142 from EXPERTISE_AREA where LABEL = 'MED';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,143 from EXPERTISE_AREA where LABEL = 'MED';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,144 from EXPERTISE_AREA where LABEL = 'MED';

-- Mer du nord manche atlantique|113;114;117
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,113 from EXPERTISE_AREA where LABEL = 'ATL';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,114 from EXPERTISE_AREA where LABEL = 'ATL';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,117 from EXPERTISE_AREA where LABEL = 'ATL';

-- Saint Pierre et Miquelon|122;124;105
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,122 from EXPERTISE_AREA where LABEL = 'SPM';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,124 from EXPERTISE_AREA where LABEL = 'SPM';
insert into EXPERTISE_AREA2LOCATION_LEVEL(EXPERTISE_AREA_FK, LOCATION_LEVEL_FK) select ID,105 from EXPERTISE_AREA where LABEL = 'SPM';

-- Final commit
commit;




