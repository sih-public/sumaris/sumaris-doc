REM INSERTING MODIFY CONTENT COLUMN TYPE ON SIH2_ADAGIO_DBA_SUMARIS_MAP.USER_EVENT

ALTER TABLE USER_EVENT ADD CONTENT_CLOB CLOB;

UPDATE USER_EVENT SET CONTENT_CLOB = CONTENT WHERE CONTENT_CLOB IS NULL;

ALTER TABLE USER_EVENT DROP COLUMN CONTENT;

ALTER TABLE USER_EVENT RENAME COLUMN CONTENT_CLOB TO CONTENT;

commit;