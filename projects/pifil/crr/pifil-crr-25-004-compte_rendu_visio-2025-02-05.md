# Compte-rendu réunion visio
## 05/02/2025

Point sur les formulaires terrain PIFIL et DOLPHINFREE

---

> Présents :
>
> - Camille YOUNSI (DGAMPA)
> - Benoit LAVENIER (EIS)
<!-- .element: class="font-size-extra-small" -->

---
# Adaptation des formulaires terrain

Suite au mail de Quitterie et Fiona du 29/01/2025, les points remontés par les OP sont revus avec la DGAMPA. 


---
## PIFIL

- Marée :
  - [x] Ajouter "Utilisation d'une caméra ?" (obligatoire)
  - [x] Vérifier que le virage se saisit bien (actuellement il n'y a plus de date saisissable)
- Captures :
  - [x] Retirer "Distance à la bouée la plus proche"

---
## DOLPHINFREE

Engin :
- [x] Équipementier - Yves Le Gall (Ifremer) en a besoin (et c'est facultatif)

Opération :
- [x] Autre disposition de la filière - à masquer sur le rapport
- Déroulement normal => à retirer
- Dysfonctionnement de l'OP => pas de commentaire
- Dysfonctionnement du dispositif technique => à mettre en facultatif

Captures
- [x] "Blessures" ? à renommer en "Blessé ?"
- [x] "Traces de capture ?" => à retirer
- [x] Agrandir la colonne "Numéro du pinger"
- 