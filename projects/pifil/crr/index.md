
## Comptes-rendu - Projets PIFIL et DOLPHINFREE

- [CRR 24/05/2024](./pifil-crr-24-001-compte_rendu_visio-2024-05-24.md) - Réunion de lancement 
- [CRR 27/09/2024](./pifil-crr-24-002-compte_rendu_visio-2024-09-27.md) - Réunion de suivi
- [CRR 11/10/2024](./pifil-crr-24-003-compte_rendu_visio-2024-10-11.md) - Réunion de suivi
- [CRR 18/10/2024](./pifil-crr-24-004-compte_rendu_visio-2024-10-18.md) - Réunion de suivi
- [CRR 31/10/2024](./pifil-crr-24-004-compte_rendu_visio-2024-10-31.md) - Réunion de suivi
- [CRR 14/11/2024](./pifil-crr-24-005-compte_rendu_visio-2024-11-14.md) - Réunion de suivi
- [CRR 21/11/2024](./pifil-crr-24-006-compte_rendu_visio-2024-11-21.md) - Réunion de travail sur le rapprochement des données
- [CRR 28/11/2024](./pifil-crr-24-007-compte_rendu_visio-2024-11-28.md) - Réunion de suivi
- [CRR 20/01/2025](./pifil-crr-25-001-compte_rendu_visio-2025-01-20-OP.md) - Réunion de travail sur les formulaires terrains - EIS / OP
- [CRR 20/01/2025](./pifil-crr-25-002-compte_rendu_visio-2025-01-20-octech.md) - Point sur la récupération des données - OcTech / Ifremer / EIS
- [CRR 29/01/2025](./pifil-crr-25-003-compte_rendu_visio-2025-01-29-octech.md) - Réunion technique - OCTech / EIS
- [CRR 05/02/2025](./pifil-crr-25-004-compte_rendu_visio-2025-02-05.md) - Point sur les formulaires terrain - DGAMPA / EIS    
- [CRR 06/02/2025](./pifil-crr-25-005-compte_rendu_visio-2025-02-06.md) - Point sur la récupération des données - Bastien Mérigot / EIS
- [CRR 25/02/2025](./pifil-crr-25-006-compte_rendu_visio-2025-02-25.md) - Point sur la procédure pour gérer les inscriptions des pros - EIS / OP

