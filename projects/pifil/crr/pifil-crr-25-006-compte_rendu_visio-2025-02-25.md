# Compte-rendu réunion visio
## 25/02/2025

Point sur la procédure pour gérer les inscriptions des pêcheurs pro participants à PIFIL et DOLPHINFREE. 

---

> Présents :
>
> - Aurelien HENNEVEUX (OP Aquitaine)
> - Quiterie SOURGET (OP LPDB)
> - Benoît LAVENIER (EIS)
> - Étienne de CHAVAGNAC (EIS)
<!-- .element: class="font-size-extra-small" -->

> Excusés :
>
> - Fiona BIGEY (OP Vendée)
> - Léni GUILLOTIN (CNPMEM)
<!-- .element: class="font-size-extra-small" -->

---
# Procédure de création de compte

- Les référents OP (ou Léni ?) doivent créer leurs navires dans l'App (Menu > Navires > Bouton "Ajouter") **s'il n'existe pas déjà**;
- Le pêcheur s'inscrit : 
  - Pensez à bien valider le compte via l'email recu (+ vérifier dans les Spams)
  - En cas de problème : support@sumaris.net
- EIS envoi chaque jour la liste des nouvelles inscriptions (capture d'écran) au référents d'OP
- Les référents OP ajoutent les droits aux comptes créés (Menu > Programmes > PIFIL2/DOLPHINFREE > Droits d'accès > Bouton "Ajouter" > Choisir le droit "Observateur")

---
# Cas particulier

Cas des navires ayant plusieurs patrons :

- Dans l'immédiat : Chaque bateau peut avoir un même compte générique bateau.
  - Au changement de patron, bien penser à synchroniser les 2 téléphones (Bouton "Synchroniser" et "Actualiser le mode hors-ligne").
  - Si non synchronisé, alors le dernier filage ne pourra pas être associé dans la marée suivante.  
- A moyen terme : 
  - EIS doit vérifier la faisabilité d'une supervision croisée (Ex: de `patron A -> patron B`, de `patron B -> patron A` et de `Armateur -> Patron(s)`);
  - Cela que chacune (patrons et l'armateur) aient leur propres comptes individuels, tout en voyant les données du bateau.
    (Actuellement un utilisateur ne verra que ses propres données saisies)

---
# Axes(s) d'amélioration

Plusieurs d'axes d'amélioration sont évoquées :
- Se connecter aux logiciels déjà utilisés à bord :
  - Exemple : Logiciel "MaxSea" (pour la position des filets);
  - Idée proposée par Olivier MERCIER (patron volontaire);
- Bouton bluetooth : type bracelet étanche (attaché au poignet);
    - EIS a avancé sur l'idée avec Vincent BADTS (Ingénieur qualité SIH - Ifremer) qui peut aider sur le sujet;
    - EIS a une point avec OCTEch et le CNPMEM pour en discuter;

