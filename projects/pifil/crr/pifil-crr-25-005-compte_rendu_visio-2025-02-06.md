# Compte-rendu réunion visio
## 06/02/2025

Point sur la récupération des données avec Bastien Mérigot / EIS

---

> Présents :
>
> - Bastien MÉRIGOT (Université de Montpellier)
> - Benoît LAVENIER (EIS)
> - Étienne de CHAVAGNAC (EIS)
<!-- .element: class="font-size-extra-small" -->

---

# Suivi du niveau de batterie des balises

## Balises

- Numéro de balise : 
  - O-CSDF-A-2346-000070
  - QR Code
- 4 heures de recharge
- 4 batteries par chargeur
- Niveau de batterie transmis au concentrateur à chaque remontée
- Signal dédié au dauphin commun
- Longueur des filets équipés
  - 200 m à 40 km

## Formulaire terrain (papier)

- Informations primordiales
  - Nombre de captures accidentelles
  - Dauphin commun ou autre espèce ?
 
## Propositions

- Prévenir le pêcheur en amont pour anticiper la recharge des batteries
  - Démonter du filet et recharger toutes les batteries simultanément
  - Remonter les balises sur le filet simultanément
  - Environ une fois par mois