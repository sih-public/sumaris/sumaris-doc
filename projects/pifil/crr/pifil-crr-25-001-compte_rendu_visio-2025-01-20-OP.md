# Compte-rendu réunion visio
## 20/01/2025

Validation des formulaires terrain PIFIL

---

> Présents :
>
> - Benoit LAVENIER (EIS)
> - Maxime DEMAREST (EIS)
> - Quitterie SOURGET (OP LPDB)
> - Aurélien HENNEVEUX (OP PDA)
> - Anne CHAUSSE (CNPMEN)
> - Fiona BIGEY (OP VENDEE)
<!-- .element: class="font-size-extra-small" -->

---
## Formulaire terrain PIFIL (1/3)
Demande de modifications :

- Marées : 
  - Revoir "Type de lieu" en "(Port)" (si uniquement un type de lieu possible)
  - Masquer "Utilisation d'un GPS"
- Engins : 
  - Enlever de l'entête "Départ de la marée" 

---
## Formulaire terrain PIFIL (2/3)

- OP : 
  - Enlever de l'entête "Départ de la marée"
  - Enlever de l'entête "Nombre total d'opérations"
  - Réduire engin / espèce cible (et découper en 2)
  - Libellé "Posision GPS Latitutde + Longiture" en "Posision GPS"
  - Libellé "Zone de pêche" à renommer "Rectangle statistique CIEM" (si uniquement un seul niveau de lieu)  
  - Mettre sur une seule ligne Filage et Virage
    - /!\ A tester sur DOLPHINFREE 

---
## Formulaire terrain PIFIL (3/3)

- Captures accidentelles : 
  - Augmenter la hauteur de la ligne 
- Relachés : 
  - A intégrer : dans le tableau captures accid. si possibles
  - Commemtaires relaché peut être supprimé


## Planning prévisionnel

Les pêcheurs sont à l'arrêt en Golfe de Cascogne. La reprise est le 20 février 2025.

**Objectif** : Avoir un formulaire terrain validé pour **le 5 février**
